package dataLayer;

import bussinessLayer.BaseProduct;
import bussinessLayer.Client;
import bussinessLayer.MenuItem;
import bussinessLayer.Order;

import java.io.File;
import java.io.IOException;
import java.io.FileWriter;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.HashSet;

public class FileWrite{
    public FileWrite(String fileName){
        myFile = new File(fileName);
        try {
            FileWriter myWriter = new FileWriter(myFile, true);
        }
        catch(IOException e){
            try {
                myFile.createNewFile();
            }catch(IOException e1){
                System.out.println("Error Creating file");
            }
        }
    }
    public void writeBill(int orderID,float price, String date, ArrayList<MenuItem> items){
        try{
            FileWriter myWriter = new FileWriter(myFile);
            myWriter.write(orderID + "\n" + date + "\n");
            for(MenuItem item : items){
                myWriter.write(item.toString() + item.computePrice());
            }
            myWriter.write("...................." + price);
            myWriter.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
    public void addUser(int id, String userName, char[] password,int ide){
        try{
            FileWriter myWriter = new FileWriter(myFile,true);
            myWriter.write(id + " " + userName + " " + String.copyValueOf(password) + " " + ide+ "\n");
            myWriter.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
    public void writeCurrentMenuData(HashSet<MenuItem> arr){
        try {
            FileWriter myWriter = new FileWriter(myFile);
            arr.stream().forEach((item) -> {
                try {
                    myWriter.write(item.toString());
                } catch (IOException e) {
                    System.out.println("Error writing file");
                }
            });
            myWriter.close();
        }
        catch(IOException e){
            e.printStackTrace();
        }
    }
    public void writeClientList(ArrayList<Client> arr){
        try {
            FileWriter myWriter = new FileWriter(myFile);
            arr.stream().forEach( (client)-> {
                try {
                    myWriter.write(client.toString());
                } catch (IOException e) {
                    System.out.println("Error writing file");
                }
            } );
            myWriter.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }
    public void writeOrderList(ArrayList<Order> arr){
        try {
            FileWriter myWriter = new FileWriter(myFile);
            arr.stream().forEach( (order)-> {
                try {
                    myWriter.write(order.toString() +"\n");
                } catch (IOException e) {
                    System.out.println("Error writing file");
                }
            } );
            myWriter.close();
        }
        catch (IOException e){
            e.printStackTrace();
        }
    }
    File myFile;
}
