package dataLayer;

import bussinessLayer.BaseProduct;
import bussinessLayer.Client;
import bussinessLayer.MenuItem;
import bussinessLayer.Order;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Serializer {

    public static int searchUser(String username,String password,String fileName){
        var ref = new Object() {
            int id = 0;
        };
        try {
            Stream<String> stream = Files.lines(Paths.get(fileName));
            stream.forEach((s)->{
                if(s.contains(username) && s.contains(password)){
                    ref.id =  Integer.parseInt(s.split(" ")[0]);
                }
            });
            stream.close();
        }
        catch(IOException e){
            e.printStackTrace();
            return -1;
        }
        return ref.id;
    }
    public static int searchUserId(String username,String password,String fileName){
        var ref = new Object() {
            int id2 = 0;
        };
        try {
            Stream<String> stream = Files.lines(Paths.get(fileName));
            stream.forEach((s)->{
                if(s.contains(username) && s.contains(password)){
                    if(s.split(" ").length == 4)
                        ref.id2 = Integer.parseInt(s.split(" ")[3]);
                }
            });
            stream.close();
        }
        catch(IOException e){
            e.printStackTrace();
            return -1;
        }
        return ref.id2;
    }
    public static HashSet<MenuItem> readBaseProducts(String fileName){
        HashSet<MenuItem> products = new HashSet<>(0);
        List<String> myList;
        try{
            Stream<String> stream = Files.lines(Paths.get(fileName)).skip(1);
            //stream = stream.distinct();
            myList = stream.collect(Collectors.toList());
            myList.forEach( (s) ->
            {
                String[] line = s.split(",");
                if(line.length != 7){
                    System.out.println("Wrong Product structure");
                }
                else {
                    BaseProduct b =new BaseProduct(line);
                    // System.out.println(line[0]);
                    products.add(b);
                }
            });
        }
        catch(IOException e){
            e.printStackTrace();
        }
        return products;
    }
    public static HashSet<MenuItem> readMemorizedProducts(String fileName){
        HashSet<MenuItem> products = new HashSet<>(0);
        List<String> myList;
        try{
            Stream<String> stream = Files.lines(Paths.get(fileName));
            //stream = stream.distinct();
            myList = stream.collect(Collectors.toList());
            myList.forEach( (s) ->
            {
                String[] line = s.split(",");
                if(line.length != 8){
                    System.out.println("Wrong Product structure");
                }
                else {
                    BaseProduct b =new BaseProduct(line);
                    // System.out.println(line[0]);
                    products.add(b);
                }
            });
        }
        catch(IOException e){
            e.printStackTrace();
        }
        return products;
    }
    public static ArrayList<Client> readClientList(String fileName){
        ArrayList<Client> clientList = new ArrayList<>(0);

        try {
            Stream<String> stream = Files.lines(Paths.get(fileName));
            List<String> myList = stream.collect(Collectors.toList());
            myList.forEach(
                    (s)-> {
                        String[] line = s.split(" ");
                        if(line.length != 3){
                            System.out.println("Wrong Client structure");
                        }
                        else{
                            Client c = new Client(Integer.parseInt(line[0]),Integer.parseInt(line[1]),Integer.parseInt(line[2]));
                            clientList.add(c);
                        }
                    }
            );
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return clientList;
    }
    public static ArrayList<Order> readOrders(String fileName){
        ArrayList<Order> arr = new ArrayList<>(0);
        try {
            Stream<String> stream = Files.lines(Paths.get(fileName));
            List<String> myList = stream.collect(Collectors.toList());
            myList.forEach(
                    (s)-> {
                        String[] line = s.split(",");
                        if(line.length != 3){
                            System.out.println("Wrong Client structure");
                        }
                        else{
                            Order o = new Order(Integer.parseInt(line[0]),Integer.parseInt(line[1]),line[2]);
                            arr.add(o);
                        }
                    }
            );
        }
        catch (IOException e){
            e.printStackTrace();
        }
        return arr;
    }
}
