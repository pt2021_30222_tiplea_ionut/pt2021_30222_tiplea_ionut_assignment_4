package presentaionLayer;

import bussinessLayer.Client;
import bussinessLayer.DeliveryService;
import dataLayer.FileWrite;
import dataLayer.Serializer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class LoginGUI implements ActionListener{
    private static JFrame f;
    private static JPanel p = new JPanel();
    private static JTextField username;
    private static JPasswordField password;
    private static JButton submit,register;
    private final FileWrite file = new FileWrite("users.txt");
    public static DeliveryService del = new DeliveryService();
    LoginGUI(){
        setFrame();
    }
    public void setFrame() {
        f = new JFrame();
        f.setSize(300, 300);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                FileWrite file = new FileWrite("file.txt");
                file.writeCurrentMenuData(del.getProducts());
                file = new FileWrite("Client.txt");
                file.writeClientList(del.getClientList());
                file = new FileWrite("orders.txt");
                file.writeOrderList(del.getOrderList());
                if (JOptionPane.showConfirmDialog(f,
                        "Are you sure you want to close this window?", "Close Window?",
                        JOptionPane.YES_NO_OPTION,
                        JOptionPane.QUESTION_MESSAGE) == JOptionPane.YES_OPTION){
                    System.exit(0);
                }
            }
        });
        username = new JTextField(10);
        password = new JPasswordField(10);
        submit = new JButton("Submit");
        register = new JButton("Register");
        submit.addActionListener(this);
        register.addActionListener(this);
        p.add(username);
        p.add(password);
        p.add(submit);
        p.add(register);
        p.setVisible(true);
        p.setLayout(new FlowLayout());
        f.getContentPane().add(p);
        f.setVisible(true);
        del.importProducts("file.txt");
    }
    public static void main(String[] args) {
        // write your code here
        LoginGUI myLogin = new LoginGUI();
        int login = 0;

    }
    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == submit){
            int login = Serializer.searchUser(username.getText(),String.copyValueOf(password.getPassword()),"Users.txt");
            int id = Serializer.searchUserId(username.getText(),String.copyValueOf(password.getPassword()),"Users.txt");
            if (login == 1) {//Admin
                AdminGUI adm = new AdminGUI(del);
            }
            if (login == 2) {//Client
                ClientGUI client = new ClientGUI(del,del.findClientById(id));
            }
        }
        if(e.getSource() == register){
            Client c = del.addClient();
            file.addUser(2,username.getText(), password.getPassword(),c.getClientID());
        }
    }
}
