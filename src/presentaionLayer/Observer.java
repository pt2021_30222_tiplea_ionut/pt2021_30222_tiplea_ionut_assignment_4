package presentaionLayer;

import presentaionLayer.EmployeeGUI;

public abstract class Observer {
    protected EmployeeGUI subject;
    public abstract void update();
}