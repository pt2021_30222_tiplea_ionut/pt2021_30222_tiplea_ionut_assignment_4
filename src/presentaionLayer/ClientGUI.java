package presentaionLayer;
import bussinessLayer.*;
import bussinessLayer.MenuItem;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class ClientGUI implements ActionListener {
    DeliveryService delServ;
    private Client c;
    private int id;
    ArrayList<MenuItem> prod = new ArrayList<>(0);
    private static JFrame f;
    private static JPanel p = new JPanel();
    private static JTextField searchBar;
    private static JButton b,addToOrder,finishOrder;
    private static JTextArea searchRez,productToAdd,productsAdded;
    private static JScrollPane scroll;
    public ClientGUI(DeliveryService delServ,Client c){
        this.delServ = delServ;
        this.c = c;
        //delServ.setBaseProducts("products.csv");
        this.setFrame();
        String rezs = "";
        for(MenuItem it : delServ.getProducts()){
            rezs += it.toString();
        }
        searchRez.setText(rezs);
    }

    public void setFrame(){
        f = new JFrame();
        f.setSize(300,300);
        //f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        searchBar = new JTextField(10);
        searchRez = new JTextArea(16,30);
        b = new JButton("Search");
        addToOrder = new JButton("Add to Order");
        finishOrder = new JButton("Create Order");
        addToOrder.addActionListener(this);
        finishOrder.addActionListener(this);
        searchRez.setEditable(false);
        scroll = new JScrollPane(searchRez);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        productToAdd = new JTextArea(3,10);
        productsAdded = new JTextArea(3,10);
        productToAdd.setBackground(new Color(0x6AFF00));
        productsAdded.setBackground(new Color(155123456));
        productsAdded.setEditable(false);
        p.add(searchBar);
        b.addActionListener(this);
        p.add(b);
        //p.add(searchRez);
        p.add(scroll);
        p.add(productToAdd);
        p.add(addToOrder);
        p.add(productsAdded);
        p.add(finishOrder);
        p.setVisible(true);
        p.setLayout(new FlowLayout());
        f.getContentPane().add(p);
        f.setVisible(true);
    }
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == b){
            String[] s = searchBar.getText().split(" ");
            ArrayList<MenuItem> rez =  delServ.searchProducts(s);
            String rezs = "";
            for(MenuItem it : rez){
                rezs += it.toString();
            }
            searchRez.setText(rezs);
        }
        if(e.getSource() == addToOrder){
            String[] prods = productToAdd.getText().split("\n");
            if(prods.length == 1){
                prod.add(new BaseProduct(prods[0].split(",")));
            }
            else{
                ArrayList<BaseProduct> b = new ArrayList<>(0);
                for(String s : prods){
                    b.add(new BaseProduct(s.split(",")));
                }
                CompositeProduct cp = new CompositeProduct(b);
                prod.add(cp);
            }
            String toAddedProds = "";
            for(MenuItem it : prod){
                toAddedProds += it.toString();
            }
            productsAdded.setText(toAddedProds);
            p.updateUI();
        }
        if(e.getSource() == finishOrder){
            delServ.createOrder(prod,c.getClientID());
            c.increaseNumberOfOrder();
            c.addValue(DeliveryService.getValueOfItems(prod));
            productToAdd.setText("");
        }
    }
}
