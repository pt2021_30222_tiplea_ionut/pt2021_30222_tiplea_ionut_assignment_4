package presentaionLayer;
import bussinessLayer.DeliveryService;
import bussinessLayer.MenuItem;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class AdminGUI implements ActionListener {
    DeliveryService delServ = new DeliveryService();

    private static  JFrame f;
    private static  JPanel p;
    private static JButton importButton, importProdButton,deleteProductsButton,changeProductButton,reportXTimesButton,
                            reportClientsButton, reportOrderDateButton,reportTimeIntervalButton;
    private static  JTextArea importProduct,deleteProduct,oldProduct,updateProduct;
    private static JTextField searchBar,reportXTimes,reportClients,orderDate,timeInterval;
    private static JButton b;
    private static JTextArea searchRez;
    private static JScrollPane scroll;
    public AdminGUI(DeliveryService delServ){
        this.delServ = delServ;
        this.setFrame();
    }
    public void setFrame(){
        //JFrame.setDefaultLookAndFeelDecorated(true);
        f = new JFrame();
        p = new JPanel();
        orderDate = new JTextField(10);
        timeInterval = new JTextField(10);
        importProduct = new JTextArea(3,10);
        oldProduct = new JTextArea(3,10);
        oldProduct.setBackground(new Color(155123456));
        updateProduct = new JTextArea(3,10);
        updateProduct.setBackground(new Color(155123456));
        importProduct.setBackground(new Color(155123456));
        deleteProduct = new JTextArea(3,10);
        deleteProduct.setBackground(new Color(155123456));
        importButton = new JButton("Import the base products");
        reportXTimesButton = new JButton("Get items ordered as specified");
        reportXTimes = new JTextField(4);
        reportClients = new JTextField(4);
        importProdButton = new JButton("Add products");
        deleteProductsButton = new JButton("Del Products");
        changeProductButton = new JButton("Change Product");
        reportClientsButton = new JButton("reportClients");
        reportOrderDateButton = new JButton("reportOrderDate");
        reportTimeIntervalButton = new JButton("reportTimeInterval");
        searchBar = new JTextField(10);
        searchRez = new JTextArea(16,30);
        b = new JButton("Search");
        searchRez.setEditable(false);
        scroll = new JScrollPane(searchRez);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        //p.add(searchRez);
        //f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        p.setLayout(new FlowLayout());
        importButton.addActionListener(this);
        importProdButton.addActionListener(this);
        changeProductButton.addActionListener(this);
        deleteProductsButton.addActionListener(this);
        reportXTimesButton.addActionListener(this);
        reportClientsButton.addActionListener(this);
        reportOrderDateButton.addActionListener(this);
        reportTimeIntervalButton.addActionListener(this);
        p.add(searchBar);
        b.addActionListener(this);
        p.add(b);
        p.add(importButton);
        p.add(importProdButton);
        p.add(importProduct);
        p.add(deleteProductsButton);
        p.add(deleteProduct);
        p.add(changeProductButton);
        p.add(oldProduct);
        p.add(updateProduct);
        p.add(reportXTimes);
        p.add(reportXTimesButton);
        p.add(reportClients);
        p.add(reportClientsButton);
        p.add(orderDate);
        p.add(reportOrderDateButton);
        p.add(timeInterval);
        p.add(reportTimeIntervalButton);
        p.add(scroll);
        p.setVisible(true);
        f.add(p);
        f.pack();
        f.setVisible(true);
    }
    public void actionPerformed(ActionEvent e){
        if(e.getSource() == importButton){
            String file = "products.csv";
            delServ.setBaseProducts(file);
        }
        if(e.getSource() == importProdButton){
            String myString = importProduct.getText();
            String[] prodList = myString.split("\n");
            delServ.addProduct(prodList);
        }
        if(e.getSource() == deleteProductsButton){
            String[] prodList = deleteProduct.getText().split("\n");
            delServ.deleteProduct(prodList);
        }
        if(e.getSource() == changeProductButton){
            String[] old = oldProduct.getText().split("\n");
            String[] update = updateProduct.getText().split("\n");
            delServ.manageProducts(old,update);
        }
        if(e.getSource() == b){
           // delServ.setBaseProducts("C://TP_lab//tema4//src//products.csv");
            String[] s = searchBar.getText().split(" ");
            ArrayList<bussinessLayer.MenuItem> rez =  delServ.searchProducts(s);
            String rezs = "";
            for(MenuItem it : rez){
                rezs += it.toString();
            }
            searchRez.setText(rezs);
        }
        if(e.getSource() == reportXTimesButton){
            int x = Integer.parseInt(reportXTimes.getText());
            searchRez.setText(delServ.reportXTimes(x));
        }
        if(e.getSource() == reportClientsButton){
            String s = reportClients.getText();
            int x = Integer.parseInt(s.split(" ")[0]);
            int y = Integer.parseInt(s.split(" ")[1]);
            searchRez.setText(delServ.reportClients(x,y));
        }
        if(e.getSource() == reportOrderDateButton){
            searchRez.setText(delServ.reportDateOrders(orderDate.getText()));
        }
        if(e.getSource() == reportTimeIntervalButton){
            String s = timeInterval.getText();
            searchRez.setText(delServ.reportTimeInterval(s));
        }
    }
}
