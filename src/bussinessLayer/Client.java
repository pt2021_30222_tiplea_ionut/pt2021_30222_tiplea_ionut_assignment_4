package bussinessLayer;

public class Client {
    public Client() {
        this.clientID = ++id;
        this.numberOfOrder = numberOfOrder;
        this.valueSpent = valueSpent;
    }
    public Client(int ClientId,int noo,int vs){
        this.clientID = ClientId;
        this.numberOfOrder = noo;
        this.valueSpent = vs;
        ++id;
    }
    public void increaseNumberOfOrder(){
        numberOfOrder++;
    }
    public void addValue(int value){
        valueSpent += value;
    }

    public int getClientID() {
        return clientID;
    }

    public void setClientID(int clientID) {
        this.clientID = clientID;
    }

    public int getNumberOfOrder() {
        return numberOfOrder;
    }

    public void setNumberOfOrder(int numberOfOrder) {
        this.numberOfOrder = numberOfOrder;
    }

    public int getValueSpent() {
        return valueSpent;
    }

    public void setValueSpent(int valueSpent) {
        this.valueSpent = valueSpent;
    }
    @Override
    public String toString(){
        StringBuilder builder = new StringBuilder();
        builder.append(this.clientID);
        builder.append(" ");
        builder.append(this.numberOfOrder);
        builder.append(" ");
        builder.append(this.valueSpent);
        builder.append("\n");
        return builder.toString();
    }
    private int clientID;
    private int numberOfOrder;
    private int valueSpent;
    private static int id = 0;
}
