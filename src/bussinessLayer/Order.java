package bussinessLayer;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Objects;

public class Order {
    public Order(){
        orderID = 0;
        clientID = 0;
        orderDate = "";
    }
    //*bumps*
    public Order(int orderID,int clientID, String orderDate){
        this.orderID = orderID;
        this.clientID = clientID;
        this.orderDate = orderDate;
        ++id;
    }
    public Order(int clientID){
        orderID = ++id;
        clientID = clientID;
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
        Date date = new Date(System.currentTimeMillis());
        orderDate = formatter.format(date);
    }
    @Override
    public int hashCode(){
        return  Objects.hash(orderID,clientID,orderDate);
    }
    @Override
    public String toString(){
        return this.orderID + "," + this.clientID + ","+ this.orderDate;
    }
    public int getOrderID() {
        return orderID;
    }
    public int getClientID() {
        return clientID;
    }
    public String getOrderDate() {
        return orderDate;
    }
    private int orderID;
    private int clientID;
    private String orderDate;
    private static int id ;
}
