package bussinessLayer;

import java.io.Serializable;
import java.util.Base64;
import java.util.Objects;

public class BaseProduct extends MenuItem implements Serializable {
    public BaseProduct(String title, double rating,double calories,double proteins,double fats,double sodium,double price){
        this.title = title;
        this.rating = rating;
        this.calories = calories;
        this.proteins = proteins;
        this.fats = fats;
        this.sodium = sodium;
        this.price = price;
    }
    public BaseProduct(String[] all){
        this.title = all[0];
        try {
            this.rating = Double.parseDouble(all[1]);
            this.calories = Double.parseDouble(all[2]);
            this.proteins = Double.parseDouble(all[3]);
            this.fats = Double.parseDouble(all[4]);
            this.sodium = Double.parseDouble(all[5]);
            this.price = Double.parseDouble(all[6]);
            if(all.length == 8)
                this.setOrderX(Integer.parseInt(all[7]));
        }catch(Exception e){
           // do nothing , if needed notify sistem where there was smth wrong
            e.printStackTrace();
            this.title = "";
            status = false;
        }
    }
    public BaseProduct(){
        this.title = "";
        this.rating = 0;
        this.calories = 0;
        this.proteins = 0;
        this.fats = 0;
        this.sodium = 0;
        this.price = 0;
    }
    @Override
    public double computePrice(){
        return this.price;
    }
    @Override
    public boolean equals(Object b){
        BaseProduct second ;
        if(!(b instanceof BaseProduct))
            return false;
        second =(BaseProduct) b;
       // if(this.title == null)
         //   return false;
        return this.title.equals(second.title) && Double.compare(this.calories, second.calories) == 0
                && Double.compare(this.sodium, second.sodium) == 0
                && Double.compare(this.price, second.price) == 0
                && Double.compare(this.fats, second.fats) == 0
                && Double.compare(this.rating, second.rating) == 0
                && Double.compare(this.proteins, second.proteins) == 0;
    }
    @Override
    public int hashCode(){
        return Objects.hash(title,rating,calories,proteins);
    }
    @Override
    public String toString(){
        StringBuilder rez = new StringBuilder();
        rez.append(title);
        rez.append(",");
        rez.append(rating);
        rez.append(",");
        rez.append(calories);
        rez.append(",");
        rez.append(proteins);
        rez.append(",");
        rez.append(fats);
        rez.append(",");
        rez.append(sodium);
        rez.append(",");
        rez.append(price);
        rez.append(",");
        rez.append(this.getOrderX());
        rez.append("\n");
        return rez.toString();
    }
    public String getTitle() {
        return title;
    }
    public boolean getStatus(){
        return this.status;
    }
    private String title;
    private boolean status = true;
    private double rating,calories,proteins,fats,sodium,price;
}
