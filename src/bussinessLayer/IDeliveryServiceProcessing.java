package bussinessLayer;

import java.util.ArrayList;

public interface IDeliveryServiceProcessing {
    public void addProduct(String[] list);
    public void manageProducts(String[] old,String[] update);
    public String reportXTimes(int x);
    public void createOrder(ArrayList<MenuItem> products,int clientID);
}
