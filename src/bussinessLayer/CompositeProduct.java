package bussinessLayer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Objects;

public class CompositeProduct extends MenuItem {
    public CompositeProduct(ArrayList<BaseProduct> p){
        for(BaseProduct base:p){
            this.p.add(base);
        }
    }
    @Override
    public double computePrice(){
        float sum = 0;
        for(BaseProduct base:p){
            sum += base.computePrice();
        }
        return sum;
    }
    @Override
    public int hashCode(){
        int code = 0;
        for(BaseProduct b : p){
            code += b.hashCode();
        }
        return code;
    }
    @Override
    public boolean equals(Object o){
        if(!(o instanceof CompositeProduct))
            return false;
        CompositeProduct cp = (CompositeProduct)o;
        if(cp.p.size() != this.p.size()){
            return false;
        }
        Iterator<BaseProduct> i1 = cp.p.iterator();
        Iterator<BaseProduct> i2 = this.p.iterator();
        while(i1.hasNext() && i2.hasNext()){
            BaseProduct b1 = i1.next();
            BaseProduct b2 = i2.next();
            if(!b1.equals(b2)){
                return false;
            }
        }
        return true;
    }
    @Override
    public String toString(){
        StringBuilder rez = new StringBuilder();
        for(BaseProduct b : p){
            rez.append(b);
            rez.append("\n");
        }
        return rez.toString();
    }
    public void addBaseProduct(BaseProduct b){
        p.add(b);
    }
    public void eliminateProduct(BaseProduct b){
        p.remove(b);
    }
    private ArrayList<BaseProduct> p = new ArrayList<>(0);
}
