package bussinessLayer;
import dataLayer.FileWrite;
import dataLayer.Serializer;

import java.util.*;

public class DeliveryService extends Observable implements IDeliveryServiceProcessing{
    private final Map<Order, ArrayList<MenuItem>> myMap = new HashMap<>(0);
    private Set<MenuItem> products = new HashSet<>(0);
    private final ArrayList<Client> clientList;
    private ArrayList<Order> orders = new ArrayList<>(0);
    public DeliveryService(){
        clientList = Serializer.readClientList("Client.txt");
        orders = Serializer.readOrders("orders.txt");
    }
    public void setBaseProducts(String file){
        this.products = Serializer.readBaseProducts(file);
    } public void importProducts(String file){
        this.products = Serializer.readMemorizedProducts(file);
    }

    @Override
    public void addProduct(String[] list){//Admin
        ArrayList<BaseProduct> productList = new ArrayList<>(0);
        for(String s : list){
            String[] aux = s.split(",");
            System.out.println(aux[0]);
            productList.add(new BaseProduct(aux));
        }
        products.addAll(productList);
        if(productList.size() > 1)
        {
            CompositeProduct cp = new CompositeProduct(productList);
            products.add(cp);
        }
        System.out.println("Product added successfully");
    }
    public void deleteProduct(String[] list){//Admin
        ArrayList<BaseProduct> productList = new ArrayList<>(0);
        for(String s : list){
            String[] aux = s.split(",");
            productList.add(new BaseProduct(aux));
        }
        if(productList.size() > 1)
        {
            CompositeProduct cp = new CompositeProduct(productList);
            products.remove(cp);
        }
        else {
            for(BaseProduct b : productList){
                products.remove(b);
                System.out.println(b.toString());
            }
        }
    }
    @Override
    public void manageProducts(String[] old, String[] update){//Admin
        deleteProduct(old);
        addProduct(update);
    }
    @Override
    public void createOrder(ArrayList<MenuItem> products,int clientID){//client
        Order current = new Order(clientID);
        myMap.put(current,products);
        float price = 0;
        for(MenuItem it : products){
            price += it.computePrice();
            for(MenuItem item : this.products){
                if(item.equals(it)){
                    item.incrementOrderX();
                }
            }
        }
        FileWrite fw = new FileWrite("bill#" + current.getOrderID() + ".txt");
        fw.writeBill(current.getOrderID(), price, current.getOrderDate(), products);
    }
    public ArrayList<MenuItem> searchProducts(String[] keyWords){//Admin + Client
        ArrayList<MenuItem> result = new ArrayList<>(0);
        products.stream().forEach((it)->{
            boolean ok = true;
            String sit = it.toString();
            for(String s : keyWords){
                if(!sit.contains(s)){
                    ok = false;
                    break;
                }
            }
            if(ok)
                result.add(it);
        });
        return result;
    }
    @Override
    public String reportXTimes(int x){
        StringBuilder rez= new StringBuilder();
        for (MenuItem item : products) {
            if (item.getOrderX() >= x) {
                rez.append(item.toString());
            }
        }
        return rez.toString();
    }

    /**
     *
     * @param date
     * @return
     */
    public String reportDateOrders(String date){
        ArrayList<Order> dateOrders = new ArrayList<>(0);
        StringBuilder rez = new StringBuilder();
        myMap.forEach((order,items) -> {
            if(order.getOrderDate().contains(date)){
                dateOrders.add(order);
            }
        });
        for(Order o : dateOrders){
            rez.append(o);
        }
        return rez.toString();
    }
    public String reportTimeInterval(String interval){
        StringBuilder rez = new StringBuilder();
        try {
            ArrayList<String> stringList = (ArrayList<String>) Collections.unmodifiableList((ArrayList<String>) myMap.keySet().stream()
                    .filter(order -> between(order.getOrderDate(), interval))
                    .map(Object::toString));
            for (String string : stringList)
                rez.append(string);
        }catch (ClassCastException e){
            ;
        }
        return rez.toString();
    }
    public Boolean between(String date, String OralInterval){
        int hour = Integer.parseInt(date.split(" ")[2].split(":")[0]) ;
        int hourIn = Integer.parseInt(OralInterval.split("-")[0]);
        int hourOut = Integer.parseInt(OralInterval.split("-")[1]);
        return hour >= hourIn && hour <= hourOut;
    }
    public String reportClients(int numOrders, int price){
        StringBuilder rez = new StringBuilder();
        myMap.forEach((order,items) -> {
            Client client = containClient(clientList,order.getClientID());
            if (client!= null){
                client.increaseNumberOfOrder();
                int value = getValueOfItems(items);
                client.addValue(value);
                clientList.add(client);
            }
        });
        for(Client client : clientList){
            if (client.getNumberOfOrder() >= numOrders && client.getValueSpent() >= price)
                rez.append(client.getClientID()).append(" ");
        }
        return rez.toString();
    }
    public Client containClient(List<Client>clientList,int idClient) {
        for (Client client: clientList) {
            if(client.getClientID() == idClient)
                return client;
        }
        return null;
    }
    public static int getValueOfItems(ArrayList<MenuItem> menuItems){
        int sum = 0;
        for(MenuItem menuItem : menuItems){
            sum += menuItem.computePrice();
        }
        return sum;
    }
    public HashSet<MenuItem> getProducts(){
        if(products instanceof HashSet)
            return (HashSet<MenuItem>) this.products;
        else
            return null;
    }
    public ArrayList<Client> getClientList(){
        return (ArrayList<Client>)clientList.clone();
    }
    public Client addClient(){
        Client c = new Client();
        clientList.add(c);
        return c;
    }
    public ArrayList<Order> getOrderList(){
        for(Map.Entry<Order,ArrayList<MenuItem>> e : myMap.entrySet()){
            orders.add(e.getKey());
        }
        return (ArrayList<Order>) orders.clone();
    }
    public Client findClientById(int id){
        for(Client c : this.clientList){
            if(c.getClientID() == id){
                return c;
            }
        }
        return null;
    }
}
